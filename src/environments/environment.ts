// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBQ-KfOeKwV2Kmj3RQucdh_fOZ-5S5dcXk",
    authDomain: "hello-jce-davidwulkan.firebaseapp.com",
    databaseURL: "https://hello-jce-davidwulkan.firebaseio.com",
    projectId: "hello-jce-davidwulkan",
    storageBucket: "hello-jce-davidwulkan.appspot.com",
    messagingSenderId: "212619787066",
    appId: "1:212619787066:web:90d39b9e83d5cb064b2e03"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
