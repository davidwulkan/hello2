import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'firebasestorage.googleapis.com/v0/b/hello-jce-davidwulkan.appspot.com/o/';
  public images:string[] = [];

  constructor() {
    this.images[0] = this.path + 'biz.JPG' + '?alt=media&token=3f6ceedb-80fd-4720-aba9-2ea295f174c0';
    this.images[1] = this.path + 'entermnt.JPG' + '?alt=media&token=7d37f89f-94b3-4693-8f78-bbdd1946c5ea';
    this.images[2] = this.path + 'politics-icon.png' + '?alt=media&token=6fcd6271-9b17-46e8-91c1-2842bb153431';
    this.images[3] = this.path + 'sport.JPG' + '?alt=media&token=4c0784fd-e19a-4bfc-9dcb-1ab236870334';
    this.images[4] = this.path + 'tech.JPG' + '?alt=media&token=57d85981-a47e-4d27-ac17-e7ef1a95dfb5';
   }
}
