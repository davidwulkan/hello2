import { MatPaginator } from '@angular/material/paginator';
import { Book } from './../interfaces/book';
import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books:Book[];  
  books$;
  userId:string;
  editstate = [];
  addBookFormOpen = false;
  firstDocumentArrived: any; //Save first document in snapshot of items received
  lastDocumentArrived: any; //Save last document in snapshot of items received
  @ViewChild(MatPaginator) paginator: MatPaginator;
  prev_strt_at:  any[] = []; //Keep the array of first document of previous pages
  

  panelOpenState = false;
  constructor(private booksService:BooksService, public AuthService:AuthService) { }

  deleteBook(id:string){
    this.booksService.deleteBook(this.userId, id);
  }

  updateBook(book:Book){
    this.booksService.updateBook(this.userId, book.id, book.title, book.author);
  }

  add(book:Book){
    this.booksService.addBook(this.userId, book.title, book.author);
  }
    
  push_prev_startAt(prev_first_doc) {
     this.prev_strt_at.push(prev_first_doc);
  }
  
  remove_last_from_start_at(){
    this.prev_strt_at.splice(this.prev_strt_at.length-1, 1);
  }
  
  get_prev_startAt(){
      return this.prev_strt_at[this.prev_strt_at.length - 1];
  }

  nextPage(){
    this.books$ = this.booksService.nextPage(this.userId, this.lastDocumentArrived);
    this.books$.subscribe(
      docs =>{
        this.lastDocumentArrived = docs[docs.length-1].payload.doc;
        this.firstDocumentArrived = docs[0].payload.doc;
        this.push_prev_startAt(this.firstDocumentArrived);

        this.books = [];
        for(let document of docs){
          const book:Book = document.payload.doc.data();
          book.id = document.payload.doc.id;
          this.books.push(book);
        }
      }
    )    
  }

  prevPage(){
    this.remove_last_from_start_at()
    this.books$ = this.booksService.prevPage(this.userId,this.get_prev_startAt());
    this.books$.subscribe(docs => {   
      this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
      this.firstDocumentArrived = docs[0].payload.doc;
      this.books = [];
        for (let document of docs) {
          const book:Book = document.payload.doc.data();
          book.id = document.payload.doc.id;
          this.books.push(book);
      }
    });
  }

  
  ngOnInit(): void {
    
    this.AuthService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.books$ = this.booksService.getBooks(this.userId);
        
        this.books$.subscribe(
          docs =>{
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            console.log('init worked');
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.firstDocumentArrived = docs[0].payload.doc;
            this.push_prev_startAt(this.firstDocumentArrived);  

            this.books = [];            
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id;
              this.books.push(book);              
            }
          }
        )
      }
    )
    
  }
  

}
