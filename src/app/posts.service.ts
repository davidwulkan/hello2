import { Posts } from './interfaces/posts';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private URL = "https://jsonplaceholder.typicode.com/posts/";
  

  constructor(private http:HttpClient) {}

  searchPostsData():Observable<Posts>{
    return this.http.get<Posts>(`${this.URL}`);
    
  }
  
  
  
}
