import { LabmdaService } from './../labmda.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-labmda',
  templateUrl: './labmda.component.html',
  styleUrls: ['./labmda.component.css']
})
export class LabmdaComponent implements OnInit {

  x:number;
  y:number;
  result:number;

  onSubmit(){
    this.lambdaService.lambda(this.x, this.y).subscribe(
      res => {
        console.log(res);
        this.result = res;       
      }
    )
  }

  constructor(private lambdaService: LabmdaService) { }

  ngOnInit(): void {
  }

}
